# Démonstrateur SonarQube/Gitlab

Cette application illustre l'utilisation du client SonarQube dans une chaîne d'intégration Gitlab.

La référence Sonarqube sur Gitlab : https://docs.sonarqube.org/latest/analysis/gitlab-integration/ 

## Pré-requis SonarQube

- Créer un projet sur Sonarqube
- Générer un jeton d'accès SonarQube autorisé sur le projet (*project token* par exemple).

## Analyse manuelle

Installer le scanner SonarQube :
https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/

Se positionner à la racine du projet et lancer l'analyse

```shell
$ export SONAR_HOST_URL=https://sonarqube-forge.din.developpement-durable.gouv.fr
$ export SONAR_TOKEN=[Jeton_SonarQube]
$ export SONAR_PROJECT_KEY=[Clé_projet_SonarQube]
$ sonar-scanner -Dsonar.projectKey=$SONAR_PROJECT_KEY
```

## Analyse via Gitlab

L'analyse via Gitlab n'est que le report de la commande en analyse manuelle

Les variables d'environnement doivent au préalable être définies dans la configuration CI/CD du projet ou l'un de ses groupes parents :
  - **SONAR_HOST_URL**=https://sonarqube-forge.din.developpement-durable.gouv.fr
  - **SONAR_TOKEN**=[Jeton_SonarQube]
  - **SONAR_PROJECT_KEY**=[Clé_projet_SonarQube]

Le fichier gitlab-ci du projet illustre la mise en oeuvre. Il ne contient que l'analyse SonarQube par soucis de simplification.
