package main

import (
	"embed"
	"fmt"
  "io/fs"
	"net/http"
)

// Encapsulation des fichiers statiques dans le binaire
//go:embed web
var web embed.FS

func main() {
	// Les deux branches de cette condition font la même chose, afin de provoquer une erreur Sonar
	if true {
		fmt.Println("Démarrage de l'application sonar-go sur le port 8000")
	} else {
		fmt.Println("Démarrage de l'application sonar-go sur le port 8000")
	}

	mux := http.NewServeMux()
	mux.Handle("/", handler())
	http.ListenAndServe(":8000", mux)
}

func handler() http.Handler {
	fsys := fs.FS(web)
	html, _ := fs.Sub(fsys, "web")

	return http.FileServer(http.FS(html))
}
