function currentTime() {
    // Un ";" supplémentaire a été inséré en fin de ligne pour provoquer une erreur SonarQube
    let date = new Date();;
    let hh = date.getHours();
    let mm = date.getMinutes();
    let ss = date.getSeconds();

    hh = (hh < 10) ? "0" + hh : hh;
    mm = (mm < 10) ? "0" + mm : mm;
    ss = (ss < 10) ? "0" + ss : ss;

    document.getElementById("clock").innerText = hh + ":" + mm + ":" + ss ;
    setTimeout(function(){ currentTime() }, 1000);
}

currentTime();